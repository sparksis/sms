@Grapes([
    @Grab(group='com.github.tomakehurst', module='wiremock', version='2.16.0'),
    @Grab(group='org.slf4j', module='slf4j-simple', version='1.7.25')
])
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.codehaus.groovy.runtime.InvokerHelper

import groovy.json.JsonOutput;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Main extends groovy.lang.Script {

    private WireMockServer wireMockServer;

    static void main(String[] args) {           
        InvokerHelper.runScript(Main, args)     
    }

    public Object run(){
        wireMockServer = new WireMockServer(options().port(8080));
        wireMockServer.start();

        bindScriptVariables();

        loadStubs();
    }

    private void bindScriptVariables() {
        getBinding().wireMockServer = this.wireMockServer;
        getBinding().jsonOut = new JsonOutput();
    }

    private void loadStubs() {
        Files.list(Paths.get("stubs")).forEachOrdered({path->
            if(path.toString().endsWith(".groovy")) {
                final message = "Loading: " + path;
                println(message);
                evaluate(path.toFile());
                println(message + " ... Complete.")
            }
        });
    }
}