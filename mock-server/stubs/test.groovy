import static com.github.tomakehurst.wiremock.client.WireMock.*;

stubFor(get(urlEqualTo("/api/sms"))
            .willReturn(aResponse()
                .withHeader("Content-Type", "text/plain")
                .withBody(jsonOut.toJson(
                    [
                        [test:"hello"],
                    ]
                ))));
