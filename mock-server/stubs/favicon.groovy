import static com.github.tomakehurst.wiremock.client.WireMock.*;

stubFor(get(urlEqualTo("/favicon.ico"))
    .willReturn(aResponse()
        .withHeader("Content-Type", "text/plain")
        .withStatus(404)
    )
);
