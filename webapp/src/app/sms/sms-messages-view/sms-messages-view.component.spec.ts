import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsMessagesViewComponent } from './sms-messages-view.component';

describe('SmsMessagesViewComponent', () => {
  let component: SmsMessagesViewComponent;
  let fixture: ComponentFixture<SmsMessagesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsMessagesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsMessagesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
