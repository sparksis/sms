import { Component, OnInit, Input } from '@angular/core';
import { SmsService } from '../services/sms.service';

@Component({
  selector: 'sms-messages-view',
  templateUrl: './sms-messages-view.component.html',
  styleUrls: ['./sms-messages-view.component.css']
})
export class SmsMessagesViewComponent implements OnInit {

  public get smsMessages() { return this.smsService.messages; }

  @Input()
  public selected;

  constructor(private smsService: SmsService) { }

  ngOnInit() { }

}
