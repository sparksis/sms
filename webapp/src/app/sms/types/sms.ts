import { SmsType } from './sms-type';

export interface Sms {
  id: string;
  date: Date;
  type: SmsType;
  did: string;
  contact: string;
  message: string;
}
