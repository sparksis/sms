import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmsService } from './services/sms.service';
import { HttpClientModule } from '@angular/common/http';
import { SmsMessageComponent } from './sms-message/sms-message.component';
import { SmsMessagesViewComponent } from './sms-messages-view/sms-messages-view.component';
import { ContactModule } from '../contact/contact.module';

import { Sms } from './types/sms';
import { SmsType } from './types/sms-type';

@NgModule({
  declarations: [SmsMessageComponent, SmsMessagesViewComponent],
  imports: [HttpClientModule, CommonModule, ContactModule],
  providers: [SmsService],
  exports: [SmsMessageComponent, SmsMessagesViewComponent],
})
export class SmsModule { }
