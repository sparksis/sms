import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { Sms } from '../types/sms';
import { SmsType } from '../types/sms-type';

@Component({
  selector: 'sms-message',
  templateUrl: './sms-message.component.html',
  styleUrls: ['./sms-message.component.css'],
})
export class SmsMessageComponent implements OnInit {

  @Input() public sms: Sms;
  @Input() public avatarUrl;

  public get text() { return this.sms.message; }
  public get type(): SmsType { return this.sms.type; }

  @HostBinding('class.received') get received() { return this.type === SmsType.INBOUND; }
  @HostBinding('class.sent') get sent() { return this.type === SmsType.OUTBOUND; }

  constructor() { }

  ngOnInit() { }

}
