import { inject, TestBed, fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';
import { Sms, SmsService, SmsType, ENDPOINT, REFRESH_PERIOD } from './sms.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Injectable } from '@angular/core';

describe('SmsService', () => {

  let smsService: SmsService;

  let httpMock: HttpTestingController;

  /**
  * Configure the module to use HttpClientTestingModule
  */
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SmsService]
    });

    smsService = TestBed.get(SmsService);
  });


  beforeEach(inject([HttpTestingController], (httpTestingController: HttpTestingController) => httpMock = httpTestingController));

  it('should make a request to the ENDPOINT defined', fakeLifecycle(() => {
    // TODO remove this bastard test after finding a fix for the issue with HttpParams being in httpMock
    let foundRequest = false;
    httpMock.match({ method: 'GET' }).forEach((req) => {
      foundRequest = true;
      expect(req.request.url).toBe(ENDPOINT);
    });
    expect(foundRequest).toBeTruthy('Could not validate request');
  }));

  it('should make requests with a api_username, api_password & method query params', fakeLifecycle(() => {
    let foundRequest = false;
    httpMock.match({ method: 'GET' }).forEach((req) => {
      foundRequest = true;
      expect(req.request.params.get('api_username')).toBe('username');
      expect(req.request.params.get('api_password')).toBe('password');
      expect(req.request.params.get('method')).toBe('getSMS');
    });
    expect(foundRequest).toBeTruthy('Didn\'t find any requests');
  }));

  it('should should be able to retrieve a list of text messages periodically', fakeLifecycle(() => {
    smsService.messages.subscribe((value) => expect(value).toBeTruthy());
    tick(REFRESH_PERIOD);
  }, { tick: false }));

  afterEach(() => httpMock.verify());

  function respondToHttpRequest(object?: Sms[] | any) {

    if (!object) {
      object = ['hello', 'goodbye'].map((message, id) => ({
        id: id + '',
        date: new Date(),
        type: '1',
        did: '5553659900',
        contact: '5553659900',
        message: message
      }));
    }
    httpMock.match({ method: 'GET' }).forEach((req) => {
      req.flush({ sms: object || [] });
    });
  }

  function fakeLifecycle(fn: () => void, options: any = { tick: true }): any {
    return fakeAsync(() => {
      smsService.ngOnInit();
      if (options.tick) {
        tick(REFRESH_PERIOD);
      }
      fn();
      respondToHttpRequest();
      discardPeriodicTasks();
    });
  }

});
