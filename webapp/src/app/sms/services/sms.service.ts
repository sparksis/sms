import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Observer } from 'rxjs';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Subscription } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Sms } from '../types/sms';
import { SmsType } from '../types/sms-type';

export const ENDPOINT = 'http://voipms.getsandbox.com/api/v1/rest.php';
export const REFRESH_PERIOD = 5000;

@Injectable()
export class SmsService implements OnInit {

  private username: string;
  private password: string;

  private messageObservable: Observable<Sms[]>;
  private messageObserver: Observer<Sms[]>;

  constructor(private httpClient: HttpClient) {
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.messageObservable = new Observable(observer => this.messageObserver = observer);
    this.messageObservable.subscribe();

    const intervalSubscription = IntervalObservable.create(REFRESH_PERIOD).subscribe(() => {
      // TODO fix naive implementation of the server watch, could produce a race condition.
      this.httpClient.get<any>(ENDPOINT, { params: this.getSmsParams }).subscribe((response) => {
        this.messageObserver.next(response.sms.map(toRealSms));
      });
    });
  }


  public get messages(): Observable<Sms[]> {
    return this.messageObservable;
  }

  private get getSmsParams() {
    return new HttpParams()
      .set('api_username', this.username)
      .set('api_password', this.password)
      .set('method', 'getSMS');
  }

}

function toRealSms(responseSms: any): Sms | {} {
  // TODO decrappifie
  const r = { type: null };
  Object.assign(r, responseSms);
  r.type = parseInt(responseSms.type, 10);
  return r;
}
