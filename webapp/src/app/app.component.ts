import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app';

  public contacts: any[] = TEST_CONTACTS;

  private _selected = this.contacts[0];
  public get selected() { return this._selected; }
  public set selected(value) { this._selected = value; }

}

const TEST_CONTACTS = [
  { name: 'Elizabeth Alley the nth ', phoneNumber: '(251) 546-9442' },
  { name: 'Anna Jones', phoneNumber: '(125) 546-4478' },
  { name: 'David Terri', phoneNumber: '(671) 925-1352' }
];
