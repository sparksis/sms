import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'contact-nav-item',
  templateUrl: './contact-nav-item.component.html',
  styleUrls: ['./contact-nav-item.component.css']
})
export class ContactNavItemComponent implements OnInit {

  @Input()
  public contact: any;

  constructor() { }

  ngOnInit() {
  }

}
