import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactNavItemComponent } from './contact-nav-item.component';

describe('ContactNavItemComponent', () => {
  let component: ContactNavItemComponent;
  let fixture: ComponentFixture<ContactNavItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactNavItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactNavItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
