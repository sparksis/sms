import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { ContactNavItemComponent } from './contact-nav-item/contact-nav-item.component';

@NgModule({
  declarations: [
    ContactDetailsComponent,
    ContactNavItemComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ContactDetailsComponent,
    ContactNavItemComponent,
  ]
})
export class ContactModule { }
