import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {

  @Input()
  public contact: any;

  public get name() { return this.contact && this.contact.name; }
  public get phoneNumber() { return this.contact && this.contact.phoneNumber; }

  constructor() { }

  ngOnInit() { }

}
