import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';

import { SmsModule } from './sms/sms.module';
import { SmsMessageComponent } from './sms/sms-message/sms-message.component';
import { ContactModule } from './contact/contact.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule, MatListModule, MatSidenavModule, BrowserAnimationsModule, SmsModule, ContactModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
